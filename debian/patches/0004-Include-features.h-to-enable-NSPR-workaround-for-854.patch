From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Fri, 10 Mar 2023 15:45:29 -0500
Subject: Include features.h to enable NSPR workaround for #854472 on  mipsel

The upstream fix was only to enable the right linkage on mipsel when
either __GLIBC__ or __UCLIBC__ were defined as preprocessor
directives.  However, __GLIBC__ isn't defined unless <features.h> has
been read, which was not the case in the files modified here.
---
 include/ike_alg.h                               | 1 +
 include/lswnss.h                                | 1 +
 include/test_buffer.h                           | 1 +
 include/x509.h                                  | 1 +
 lib/libswan/ike_alg_aes.c                       | 1 +
 lib/libswan/ike_alg_encrypt_chacha20_poly1305.c | 1 +
 lib/libswan/ike_alg_sha2.c                      | 1 +
 lib/libswan/lswnss.c                            | 1 +
 lib/libswan/pubkey_der_to_pubkey_content.c      | 1 +
 lib/libswan/pubkey_ecdsa.c                      | 1 +
 lib/libswan/pubkey_rsa.c                        | 1 +
 lib/libswan/rnd.c                               | 1 +
 lib/libswan/secret_pubkey_stuff_to_pubkey_der.c | 1 +
 programs/cavp/test_gcm.c                        | 1 +
 programs/pluto/ikev2_eap.c                      | 1 +
 programs/pluto/root_certs.c                     | 1 +
 16 files changed, 16 insertions(+)

diff --git a/include/ike_alg.h b/include/ike_alg.h
index 2730200..0dec9f0 100644
--- a/include/ike_alg.h
+++ b/include/ike_alg.h
@@ -17,6 +17,7 @@
 #define IKE_ALG_H
 
 #include <stdbool.h>	/* for bool */
+#include <features.h>
 #include <pk11pub.h>
 #include "shunk.h"
 #include "ietf_constants.h"
diff --git a/include/lswnss.h b/include/lswnss.h
index 64c4f12..9fecba9 100644
--- a/include/lswnss.h
+++ b/include/lswnss.h
@@ -17,6 +17,7 @@
 #ifndef LSWNSS_H
 #define LSWNSS_H
 
+#include <features.h>
 #include <prerror.h>		/* for PRErrorCode, for PR_GetError() */
 #include <pk11pub.h>
 
diff --git a/include/test_buffer.h b/include/test_buffer.h
index 8093ea3..dd10a7b 100644
--- a/include/test_buffer.h
+++ b/include/test_buffer.h
@@ -17,6 +17,7 @@
 #define TEST_BUFFER_H
 
 #include <stdbool.h>
+#include <features.h>
 #include <pk11pub.h>
 
 #include "chunk.h"
diff --git a/include/x509.h b/include/x509.h
index 25a8beb..b78a501 100644
--- a/include/x509.h
+++ b/include/x509.h
@@ -25,6 +25,7 @@
 #ifndef _X509_H
 #define _X509_H
 
+#include <features.h>
 #include <cert.h>		/* dep:libnss3-dev */
 
 #include "verbose.h"
diff --git a/lib/libswan/ike_alg_aes.c b/lib/libswan/ike_alg_aes.c
index d5ecb61..b5c0a2c 100644
--- a/lib/libswan/ike_alg_aes.c
+++ b/lib/libswan/ike_alg_aes.c
@@ -19,6 +19,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <blapit.h>		/* for AES_BLOCK_SIZE */
 
 #include "constants.h"		/* for BYTES_FOR_BITS() */
diff --git a/lib/libswan/ike_alg_encrypt_chacha20_poly1305.c b/lib/libswan/ike_alg_encrypt_chacha20_poly1305.c
index 7384c35..7f58e27 100644
--- a/lib/libswan/ike_alg_encrypt_chacha20_poly1305.c
+++ b/lib/libswan/ike_alg_encrypt_chacha20_poly1305.c
@@ -13,6 +13,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <pkcs11.h>
 
 #include "ike_alg.h"
diff --git a/lib/libswan/ike_alg_sha2.c b/lib/libswan/ike_alg_sha2.c
index 1809b77..8b0ad8b 100644
--- a/lib/libswan/ike_alg_sha2.c
+++ b/lib/libswan/ike_alg_sha2.c
@@ -18,6 +18,7 @@
  *
  */
 
+#include <features.h>
 #include <pkcs11t.h>
 
 #include "constants.h"		/* for BYTES_FOR_BITS() */
diff --git a/lib/libswan/lswnss.c b/lib/libswan/lswnss.c
index 4baf4ab..feb4fce 100644
--- a/lib/libswan/lswnss.c
+++ b/lib/libswan/lswnss.c
@@ -14,6 +14,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <nspr.h>
 #include <pk11pub.h>
 #include <secmod.h>
diff --git a/lib/libswan/pubkey_der_to_pubkey_content.c b/lib/libswan/pubkey_der_to_pubkey_content.c
index 7d9b27e..279bbb9 100644
--- a/lib/libswan/pubkey_der_to_pubkey_content.c
+++ b/lib/libswan/pubkey_der_to_pubkey_content.c
@@ -13,6 +13,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <keyhi.h>
 
 #include "lswnss.h"
diff --git a/lib/libswan/pubkey_ecdsa.c b/lib/libswan/pubkey_ecdsa.c
index dd4baef..ee55768 100644
--- a/lib/libswan/pubkey_ecdsa.c
+++ b/lib/libswan/pubkey_ecdsa.c
@@ -25,6 +25,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <pk11pub.h>
 #include <cryptohi.h>
 #include <keyhi.h>
diff --git a/lib/libswan/pubkey_rsa.c b/lib/libswan/pubkey_rsa.c
index c7416c8..36db894 100644
--- a/lib/libswan/pubkey_rsa.c
+++ b/lib/libswan/pubkey_rsa.c
@@ -25,6 +25,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <cryptohi.h>
 #include <keyhi.h>
 
diff --git a/lib/libswan/rnd.c b/lib/libswan/rnd.c
index c99a0d3..d6d1ea8 100644
--- a/lib/libswan/rnd.c
+++ b/lib/libswan/rnd.c
@@ -19,6 +19,7 @@
  *
  */
 
+#include <features.h>
 #include <pk11pub.h>
 
 #include "rnd.h"
diff --git a/lib/libswan/secret_pubkey_stuff_to_pubkey_der.c b/lib/libswan/secret_pubkey_stuff_to_pubkey_der.c
index a23abb7..e91fb30 100644
--- a/lib/libswan/secret_pubkey_stuff_to_pubkey_der.c
+++ b/lib/libswan/secret_pubkey_stuff_to_pubkey_der.c
@@ -13,6 +13,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <keyhi.h>
 
 #include "lswnss.h"
diff --git a/programs/cavp/test_gcm.c b/programs/cavp/test_gcm.c
index 08c4286..4b3371d 100644
--- a/programs/cavp/test_gcm.c
+++ b/programs/cavp/test_gcm.c
@@ -14,6 +14,7 @@
  * for more details.
  */
 
+#include <features.h>
 #include <pk11pub.h>
 #include <blapit.h>
 
diff --git a/programs/pluto/ikev2_eap.c b/programs/pluto/ikev2_eap.c
index a238a12..2a0c92a 100644
--- a/programs/pluto/ikev2_eap.c
+++ b/programs/pluto/ikev2_eap.c
@@ -25,6 +25,7 @@
  * - use helper thread to do NSS crypto?
  */
 
+#include <features.h>
 #include <ssl.h>
 #include <prmem.h>
 
diff --git a/programs/pluto/root_certs.c b/programs/pluto/root_certs.c
index edecf6e..8a96f4d 100644
--- a/programs/pluto/root_certs.c
+++ b/programs/pluto/root_certs.c
@@ -16,6 +16,7 @@
  *
  */
 
+#include <features.h>
 #include <cert.h>
 
 #include "defs.h"
